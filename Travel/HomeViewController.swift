//
//  HomeViewController.swift
//  Travel
//
//  Created by Alex Layton on 02/11/2016.
//  Copyright © 2016 alexlayton. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController {

    // MARK: Properties
    
    var deals: [Deal]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    private let activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    // MARK: UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.refresh(refreshControl:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicatorView)
        
        refresh()
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Download images for new size
        tableView.reloadData()
    }
    
    // MARK: UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
       return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deals?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DealTableViewCell.reuseIdentifier, for: indexPath) as? DealTableViewCell else { fatalError("Failed to dequeue DealTableViewCell") }
        cell.deal = deals?[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 88.0
    }
    
    // MARK:
    
    @objc private func refresh(refreshControl: UIRefreshControl? = nil) {
        activityIndicatorView.startAnimating()
        let _ = DealsClient.deals { [unowned self] (deals, error) in
            self.deals = deals
            self.activityIndicatorView.stopAnimating()
            refreshControl?.endRefreshing()
            
            if let error = error {
                self.showError(error)
            }
        }
    }
    
    private func showError(_ error: DealsClient.DealsError) {
        let title: String
        let message: String
        
        switch error {
        case .invalidJSON:
            title = "Invalid JSON"
            message = "Unable to parse JSON Response"
        case .invalidURL:
            title = "Invalid URL"
            message = "Unable to perform Request"
        case .requestError(let description):
            title = "Request Error"
            message = description
        case .APIError(let description):
            title = "API Error"
            message = description
        }
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let retryAction = UIAlertAction(title: "Retry", style: .default) { _ in
            self.refresh()
        }
        alertController.addAction(cancelAction)
        alertController.addAction(retryAction)
        present(alertController, animated: true, completion: nil)
    }

}

