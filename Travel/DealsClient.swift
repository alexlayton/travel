//
//  DealsClient.swift
//  Travel
//
//  Created by Alex Layton on 02/11/2016.
//  Copyright © 2016 alexlayton. All rights reserved.
//

import Foundation
import UIKit

struct DealsClient {

    // MARK: Types
    
    typealias DealsCompletionBlock = ([Deal]?, DealsError?) -> Void
    
    typealias ImageCompletionBlock = (UIImage?) -> Void
    
    enum DealsError: Error {
        case invalidURL
        case invalidJSON
        case requestError(String)
        case APIError(String)
    }
    
    // MARK: Properties
    
    private static let imageCache = NSCache<NSString, UIImage>()
    
    // MARK:
    
    static func deals(_ completionBlock: @escaping DealsCompletionBlock) -> URLSessionDataTask? {
        
        guard let url = URL(string: "https://www.travelrepublic.co.uk/api/hotels/deals/search?fields=Aggregates.HotelsByChildDestination") else {
            completionBlock(nil, .invalidURL)
            return nil
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let jsonString = "{\"CheckInDate\":\"2017-01-10T00:00:00.000Z\", \"Flexibility\":3, \"Duration\":7, \"Adults\":2, \"DomainId\":1, \"CultureCode\":\"en-gb\", \"CurrencyCode\":\"GBP\", \"OriginAirports\":[\"LHR\",\"LCY\",\"LGW\",\"LTN\",\"STN\",\"SEN\"], \"FieldFlags\":8143571, \"IncludeAggregates\":true}"
        urlRequest.httpBody = jsonString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: urlRequest) { data, _, error in
            
            if let error = error {

                DispatchQueue.main.async { completionBlock(nil, .requestError(error.localizedDescription)) }
                
            } else if let data = data {
                
                let json = try? JSONSerialization.jsonObject(with: data, options: [])
                let jsonDictionary = json as? [String: Any]
                
                if let message = jsonDictionary?["Message"] as? String {
                    DispatchQueue.main.async { completionBlock(nil, .APIError(message)) }
                    return
                }
                
                let aggregates = jsonDictionary?["Aggregates"] as? [String: Any]
                guard let dealsDictionary = aggregates?["HotelsByChildDestination"] as? [String: Any] else {
                    DispatchQueue.main.async { completionBlock(nil, .invalidJSON) }
                    return
                }
                
                var deals = [Deal]()
                for dealValue in dealsDictionary {
                    if let deal = Deal(key: dealValue.key, value: dealValue.value) {
                        deals.append(deal)
                    }
                }
                deals.sort { $0.position < $1.position }
                DispatchQueue.main.async { completionBlock(deals, nil) }
            }
        }
        task.resume()
        return task
    }
    
    static func image(for deal: Deal, withSize size: CGSize, completionBlock: @escaping ImageCompletionBlock) -> URLSessionDataTask? {
        
        guard let url = URL(string: "https://d2f0rb8pddf3ug.cloudfront.net/api2/destination/images/getfromobject?id=\(deal.id)&type=\(deal.type)&useDialsImages=true&width=\(Int(size.width))&height=\(Int(size.height))") else {
            completionBlock(nil)
            return nil
        }
        
        if let cachedImage = imageCache.object(forKey: url.absoluteString as NSString) {
            DispatchQueue.main.async { completionBlock(cachedImage) }
            return nil
        }
        
        let task = URLSession.shared.dataTask(with: url) { data, _, _ in
            guard let data = data else {
                DispatchQueue.main.async { completionBlock(nil) }
                imageCache.removeObject(forKey: url.absoluteString as NSString)
                return
            }
            let image = UIImage(data: data)
            
            if let image = image {
                imageCache.setObject(image, forKey: url.absoluteString as NSString)
            }
            
            DispatchQueue.main.async { completionBlock(image) }
        }
        task.resume()
        return task
    }
    
}
