//
//  DealTableViewCell.swift
//  Travel
//
//  Created by Alex Layton on 02/11/2016.
//  Copyright © 2016 alexlayton. All rights reserved.
//

import UIKit

class DealTableViewCell: UITableViewCell {

    // MARK: Properties
    
    static let reuseIdentifier = "DealTableViewCell"
    
    var deal: Deal? {
        didSet {
            configure()
        }
    }
    
    var imageTask: URLSessionDataTask?
    
    private lazy var backgroundImageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var countLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .title3)
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var fromLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "From"
        label.textColor = UIColor.white
        return label
    }()
    
    private lazy var priceLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    // MARK: Initializers
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        contentView.addSubview(backgroundImageView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(countLabel)
        contentView.addSubview(fromLabel)
        contentView.addSubview(priceLabel)
        
        NSLayoutConstraint.activate([
            
            // Background Image View
            backgroundImageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            backgroundImageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            backgroundImageView.topAnchor.constraint(equalTo: contentView.topAnchor),
            backgroundImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            // Title Label
            titleLabel.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
            titleLabel.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor),
            
            // Count Label
            countLabel.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 16.0),
            countLabel.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            
            // From Label
            fromLabel.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
            fromLabel.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),
            
            // Price Label
            priceLabel.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),
            priceLabel.bottomAnchor.constraint(equalTo: fromLabel.bottomAnchor)
            ])
    }
    
    // MARK:
    
    private func configure() {
        imageTask?.cancel()
        
        guard let deal = deal else { return }
        
        titleLabel.text = deal.title
        countLabel.text = "(\(deal.count))"
        
        let priceString = String(format: "£%.2f", deal.minimumPrice)
        let attributedString = NSMutableAttributedString(string: priceString, attributes: [ NSFontAttributeName : UIFont.preferredFont(forTextStyle: .title3), NSForegroundColorAttributeName : UIColor.orange ])
        let ppString = NSAttributedString(string: " pp", attributes: [ NSFontAttributeName : UIFont.preferredFont(forTextStyle: .caption1), NSForegroundColorAttributeName : UIColor.white ])
        attributedString.append(ppString)
        priceLabel.attributedText = attributedString
        
        imageTask = DealsClient.image(for: deal, withSize: self.frame.size) { [weak self] (image) in
            self?.backgroundImageView.image = image
        }
    }
}
