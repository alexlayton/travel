//
//  Deal.swift
//  Travel
//
//  Created by Alex Layton on 02/11/2016.
//  Copyright © 2016 alexlayton. All rights reserved.
//

import Foundation

struct Deal {

    let id: String
    let type: String
    let title: String
    let count: Int
    let minimumPrice: Double
    let position: Int
    
    init?(key: String, value: Any) {
        
        let keyComponents = key.components(separatedBy: "|")
        
        guard let type = keyComponents.first,
            let id = keyComponents.last,
            let json = value as? [String: Any],
            let title = json["Title"] as? String,
            let count = json["Count"] as? Int,
            let postion = json["Position"] as? Int,
            let minimumPrice = json["MinPrice"] as? Double else {
            return nil
        }
        
        self.id = id
        self.type = type
        self.title = title
        self.count = count
        self.position = postion
        self.minimumPrice = minimumPrice
        
    }
    
}
